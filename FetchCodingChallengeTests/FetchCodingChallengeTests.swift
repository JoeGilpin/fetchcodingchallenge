import XCTest
import SwiftUI
import UIKit

@testable import FetchCodingChallenge

final class FetchCodingChallengeTests: XCTestCase {

    func testDecoder() throws {
        let mealModel = MealMock.meal

        let decodedMeal = try? JSONDecoder().decode(MealModel.self, from: mockMealData)

        XCTAssertEqual(mealModel, decodedMeal)
    }

}

struct MealMock {
    static let meal = MealModel(
        ingredients: [
            Ingredient(ingredient: "Milk", measurement: "1 cup"),
            Ingredient(ingredient: "Flour", measurement: "1.5 cups"),
            Ingredient(ingredient: "Eggs", measurement: "2")
        ],
        strMeal: "Test Meal",
        strInstructions: "Mix it"
    )
}

private let mockMealData = Data("""
{
    "strMeal": "Test Meal",
    "strInstructions": "Mix it",
    "strIngredient1": "Milk",
    "strIngredient2": "Flour",
    "strIngredient3": "",
    "strIngredient4": "",
    "strIngredient5": null,
    "strIngredient6": "",
    "strIngredient7": "",
    "strIngredient8": "",
    "strIngredient9": "Eggs",
    "strIngredient10": "",
    "strIngredient11": "",
    "strIngredient12": "",
    "strIngredient13": "",
    "strIngredient14": "",
    "strIngredient15": "",
    "strIngredient16": "",
    "strIngredient17": "",
    "strIngredient18": "",
    "strIngredient19": "",
    "strIngredient20": "",
    "strMeasure1": "1 cup",
    "strMeasure2": "1.5 cups",
    "strMeasure3": " ",
    "strMeasure4": " ",
    "strMeasure5": null,
    "strMeasure6": " ",
    "strMeasure7": " ",
    "strMeasure8": " ",
    "strMeasure9": "2",
    "strMeasure10": " ",
    "strMeasure11": " ",
    "strMeasure12": " ",
    "strMeasure13": " ",
    "strMeasure14": " ",
    "strMeasure15": " ",
    "strMeasure16": " ",
    "strMeasure17": " ",
    "strMeasure18": " ",
    "strMeasure19": " ",
    "strMeasure20": " ",
}
""".utf8)
