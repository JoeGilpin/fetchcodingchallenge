import SwiftUI

extension URL {

    static func desserts() -> URL {
        URL(string: "https://themealdb.com/api/json/v1/1/filter.php?c=Dessert")!
    }

    static func meal(id: String) -> URL {
        URL(string: "https://themealdb.com/api/json/v1/1/lookup.php?i=\(id)")!
    }
}
