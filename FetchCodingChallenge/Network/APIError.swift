enum APIError: Error {
    case failedDecoding(String)

    var errorDescription: String? {
        switch self {
        case .failedDecoding(let errorString):
            return "FAILED DECODING: \(errorString)"
        }
    }
}
