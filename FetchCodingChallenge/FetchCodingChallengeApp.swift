import SwiftUI

@main
struct FetchCodingChallengeApp: App {
    var body: some Scene {
        WindowGroup {
            DessertsListView()
        }
    }
}
