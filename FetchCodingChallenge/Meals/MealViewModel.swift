import SwiftUI

protocol MealViewModelProtocol: ObservableObject {

    var mealModel: MealModel? { get }
    var mealAPIError: APIError? { get }

    func fetchMeal(id: String)

}

class MealViewModel: MealViewModelProtocol {

    // model of the meal
    @Published var mealModel: MealModel?
    /// error message for the API (nil if no error)
    @Published var mealAPIError: APIError?

    /// Attempts to fetch the meal for the specified id. If it fails, it sets the mealAPIError.
    /// - Parameter id: id of the meal to be fetched
    func fetchMeal(id: String) {
        let mealURL = URL.meal(id: id)
        NetworkManager.shared.fetch(from: mealURL) { (result: Result<MealsModel, Error>) in
            switch result {
            case .success(let data):
                DispatchQueue.main.async {
                    self.mealModel = data.meals[0]
                }
            case .failure(let error):
                self.mealAPIError = .failedDecoding(error.localizedDescription)
            }
        }
    }
}
