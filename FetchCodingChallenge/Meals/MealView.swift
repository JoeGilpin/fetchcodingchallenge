import SwiftUI

struct MealView<MealViewModel: MealViewModelProtocol>: View {

    let id: String
    @StateObject var vm: MealViewModel

    var body: some View {
        ScrollView {
            if let error = vm.mealAPIError {
                Text("\(error.errorDescription!)")
            } else {
                if let mealModel = vm.mealModel {
                    Text("\(mealModel.strMeal)").textStyle(Header1Style())
                    Spacer()
                    VStack(alignment: .leading) {
                        ForEach(mealModel.ingredients, id: \.self) { ingredient in
                            Text("\(ingredient.ingredient): \(ingredient.measurement)").textStyle(Body1Style())
                        }
                    }.frame(maxWidth: .infinity, alignment: .leading)
                    Spacer()
                    Text("\(mealModel.strInstructions)").textStyle(Body2Style())
                }
            }
        }
        .padding()
        .onAppear(perform: {
            vm.fetchMeal(id: id)
        })
    }

}

#Preview {
    class MockMealViewModel: MealViewModel {
        override func fetchMeal(id: String) {
            self.mealModel = MealModel(
                ingredients: [
                    Ingredient(ingredient: "First", measurement: "Some tbsp"),
                    Ingredient(ingredient: "Second", measurement: "Less tbsp"),
                    Ingredient(ingredient: "Third", measurement: "Some tsp"),
                    Ingredient(ingredient: "Fourth", measurement: "Less tsp"),
                    Ingredient(ingredient: "Fifth", measurement: "A pinch")
                ],
                strMeal: "Dessert Name",
                strInstructions: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
            )
        }
    }

    return MealView(id: "", vm: MockMealViewModel())
}
