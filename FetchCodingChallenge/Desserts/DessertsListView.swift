import SwiftUI

struct DessertsListView: View {

    @StateObject var dessertsViewModel = DessertsListViewModel()

    var body: some View {
        if let error = dessertsViewModel.dessertsAPIError {
            Text("\(error.errorDescription!)")
        } else {
            NavigationView {
                List {
                    ForEach(dessertsViewModel.desserts, id: \.self) { dessert in
                        NavigationLink(dessert.strMeal) {
                            MealView(id: dessert.idMeal, vm: MealViewModel())
                        }
                    }
                }
                .navigationTitle("Desserts")
            }

            .onAppear(perform: {
                dessertsViewModel.fetchDesserts()
            })
        }
    }
}

#Preview {
    class MockDessertsListViewModel: DessertsListViewModel {
        override func fetchDesserts() {
            self.desserts = [
                DessertModel(idMeal: "", strMeal: "Meal 1"),
                DessertModel(idMeal: "", strMeal: "Meal 2"),
                DessertModel(idMeal: "", strMeal: "Meal 3"),
                DessertModel(idMeal: "", strMeal: "Meal 4"),
                DessertModel(idMeal: "", strMeal: "Meal 5")
            ]
        }
    }

    return DessertsListView(dessertsViewModel: MockDessertsListViewModel())
}
