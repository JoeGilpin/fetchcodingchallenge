struct DessertsModel: Decodable, Hashable {
    let meals: [DessertModel]
}

struct DessertModel: Decodable, Hashable {
    let idMeal: String
    let strMeal: String
}
