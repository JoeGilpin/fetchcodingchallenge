import SwiftUI

class DessertsListViewModel: ObservableObject {
    
    /// model of array of desserts
    @Published var desserts: [DessertModel] = []
    /// error message for the API (nil if no error)
    @Published var dessertsAPIError: APIError?
    
    /// Attempts to fetch the list of desserts. If it fails, it sets the dessertsAPIError.
    func fetchDesserts() {
        let url = URL.desserts()
        NetworkManager.shared.fetch(from: url) { (result: Result<DessertsModel, Error>) in
          switch result {
          case .success(let data):
              DispatchQueue.main.async {
                  self.desserts = data.meals.sorted(by: { $0.strMeal < $1.strMeal })
              }
          case .failure(let error):
              self.dessertsAPIError = .failedDecoding(error.localizedDescription)
          }
        }
    }
}
