import SwiftUI

struct Typography {
    static let header1: CGFloat = 32

    static let body1: CGFloat = 18
    static let body2: CGFloat = 16

}

extension Text {
    func textStyle<Style: ViewModifier>(_ style: Style) -> some View {
        ModifiedContent(content: self, modifier: style)
    }
}

struct Header1Style: ViewModifier {
    @Environment(\.sizeCategory) var sizeCategory
    func body(content: Content) -> some View {
        content
            .font(.system(size: UIFontMetrics.default.scaledValue(for: Typography.header1)))
            .multilineTextAlignment(.center)
    }
}

struct Body1Style: ViewModifier {
    @Environment(\.sizeCategory) var sizeCategory
    func body(content: Content) -> some View {
        content
            .font(.system(size: UIFontMetrics.default.scaledValue(for: Typography.body1)))
    }
}

struct Body2Style: ViewModifier {
    @Environment(\.sizeCategory) var sizeCategory
    func body(content: Content) -> some View {
        content
            .font(.system(size: UIFontMetrics.default.scaledValue(for: Typography.body2)))
    }
}

#Preview {
    VStack {
        Text("Header 1").textStyle(Header1Style())
        Text("Body 1").textStyle(Body1Style())
        Text("Body 2").textStyle(Body2Style())
    }
}
