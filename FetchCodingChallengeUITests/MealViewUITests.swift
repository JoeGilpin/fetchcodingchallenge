import XCTest

final class MealViewUITests: XCTestCase {

    var app: XCUIApplication!
    
    override func setUpWithError() throws {
        try super.setUpWithError()

        app = XCUIApplication()
        app.launch()
    }
    
    override func tearDownWithError() throws {
            try super.tearDownWithError()
            app.terminate()
    }

    func testNavigationToMealView() throws {
        XCUIApplication().collectionViews/*@START_MENU_TOKEN@*/.staticTexts["Apam balik"]/*[[".cells",".buttons[\"Apam balik\"].staticTexts[\"Apam balik\"]",".staticTexts[\"Apam balik\"]"],[[[-1,2],[-1,1],[-1,0,1]],[[-1,2],[-1,1]]],[0]]@END_MENU_TOKEN@*/.tap()

        XCTAssert(app.staticTexts.element(matching: .staticText, identifier: "Apam balik").exists)
        XCTAssert(app.staticTexts.element(matching: .staticText, identifier: "Milk: 200ml").exists)
        XCTAssert(app.staticTexts.element(matching: .staticText, identifier: "Eggs: 2").exists)
        XCTAssert(app.staticTexts.element(matching: .staticText, identifier: "Flour: 1600g").exists)
        XCTAssert(app.staticTexts.element(matching: .staticText, identifier: "Baking Powder: 3 tsp").exists)
        XCTAssert(app.staticTexts.element(matching: .staticText, identifier: "Salt: 1/2 tsp").exists)
        XCTAssert(app.staticTexts.element(matching: .staticText, identifier: "Unsalted Butter: 25g").exists)
        XCTAssert(app.staticTexts.element(matching: .staticText, identifier: "Sugar: 45g").exists)
        XCTAssert(app.staticTexts.element(matching: .staticText, identifier: "Peanut Butter: 3 tbs").exists)

        // Instructions are tested with a predicate to avoid maximum length querying error
        let instructionsStart = "Mix milk, oil and egg together. Sift flour, baking powder and salt into the mixture. Stir well until all ingredients are combined evenly."
        let predicate = NSPredicate(format: "label CONTAINS[c] %@", instructionsStart)
        XCTAssertTrue(app.staticTexts.element(matching: predicate).exists)
    }
}
