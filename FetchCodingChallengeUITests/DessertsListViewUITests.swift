import XCTest

final class DessertsListViewUITests: XCTestCase {

    var app: XCUIApplication!

    override func setUpWithError() throws {
        try super.setUpWithError()

        app = XCUIApplication()
        app.launch()
    }

    override func tearDownWithError() throws {
            try super.tearDownWithError()
            app.terminate()
    }

    func testLoadingSuccess() throws {
        XCTAssertTrue(app.buttons["Apam balik"].exists)
        XCTAssertTrue(app.buttons["Apple & Blackberry Crumble"].exists)
    }
}
